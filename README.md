Table of Contents
-----------------
1. [Description](#description)
2. [Pipeline](#pipeline)
3. [License](#license)
4. [Disclaimer](#disclaimer)
5. [Author](#author-information)

Description
=========

Repo provides a sample mock deployment of RESTfull API application with flask frontend and mongodb backend. Application is packaged in container and deployment release further described in a helm chart. Helm release creates deployment of flask application and mongodb taken from [bitnami official repo](https://github.com/bitnami/charts/tree/master/bitnami/mongodb) (all credits go to them).

Helm chart also deploys 2 services: 1 ClusterIP for front-end to backend communication and second one LoadBalancer -> for native EKS integration that creates classical LB and public DNS record.

Folder 'tests' is having an python REST API script that can communicate to application and create, delete users taken from inputs .csv file located in a subfolder. This script and data are used for functional testing within the pipeline.

Pipeline
------------

Pipeline results and jobs were run on EKS cluster that was created by terraform from rest-api-app/infra repository.

A multistage gitlab-ci pipeline is built using kubernetes as executor and consists of:
- Code style stage test for python code
- Deploy stage: using helm and deploying with kubernetes executor within EKS cluster
- Artifacts stage: to retrieve automatically generated public DNS record of aws classic LB HTTP endpoint, saving it in a variable file (current limitation of gitlab ci-runner)
- Functional test stage: triggering inputs scripts to actually populate newly deployed application with data via OpenAPI and clean it afterwards.
- Scan stage:
  -  [kubesec-sast](https://github.com/controlplaneio/kubesec): using native gitlab-ci integration with kubesec static vulnerability scan over k8s manifests
  - [semgrep-test](https://github.com/marketplace/actions/semgrep-action): using native gitlab-ci integration with semgrep
  - [bandit-sast](https://pypi.org/project/bandit/): python static code vulnerability analysis

Disclaimer
-------

TLS encryption for public AWS endpoint (classic LB) was not in a scope of this project. This is a mockup application that is not designed for production use. Production release must have TLS encryption over https and would have cert-manager deployment with automatic certificate generation as well as aws ingress controller instead of classic LB.

Mongodb is having authenticaiton turned off which is **NOT** how it should be run with real data in production, author's intention is to demonstrate an ability of a stack and do a mock up excercise, rather than do production-grade setup. Mongodb is also configured to be run in standalone mode, in production use case one would want to read about mongodb clusterisation and concept of [Replica Set](https://docs.mongodb.com/manual/administration/replica-set-deployment/) (not to be confused with k8s replicas).


License
-------

GNU GPL license


Author Information
------------------

Dmitry Rachkov
