import csv
import json


def transform_csv(csv_file):
    data = []
    with open(csv_file) as f:
        for row in csv.DictReader(f):
            data.append(row)
    json_data = json.dumps(data, indent=4, sort_keys=True)
    return json_data
